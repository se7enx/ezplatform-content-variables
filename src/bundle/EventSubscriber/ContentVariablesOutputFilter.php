<?php

namespace ContextualCode\EzPlatformContentVariablesBundle\EventSubscriber;

use ContextualCode\EzPlatformContentVariablesBundle\Service\Handler\Variable;
use eZ\Bundle\EzPublishIOBundle\BinaryStreamResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ContentVariablesOutputFilter implements EventSubscriberInterface
{
    public const WRAPPER = '#';

    /** @var Variable */
    protected $variableHandler;

    public function __construct(Variable $variableHandler)
    {
        $this->variableHandler = $variableHandler;
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if ($this->doSupport($event)) {
            $response = $event->getResponse();
            $response->setContent($this->replaceContentVariables($response->getContent()));
        }
    }

    protected function doSupport(ResponseEvent $event): bool
    {
        if (!$event->isMasterRequest()) {
            return false;
        }

        $response = $event->getResponse();
        if (
            $response instanceof StreamedResponse
            || $response instanceof RedirectResponse
            || $response instanceof JsonResponse
            || $response instanceof BinaryFileResponse
            || $response instanceof BinaryStreamResponse
        ) {
            return false;
        }

        $route = $event->getRequest()->attributes->get('_route');
        if (!in_array($route, $this->getSupportedRoutes(), true)) {
            return false;
        }

        return true;
    }

    protected function getSupportedRoutes(): array
    {
        return [
            'ez_urlalias',
            '_ezpublishLocation',
            '_ezpublishPreviewContent',
        ];
    }

    protected function replaceContentVariables(string $content): string
    {
        /** @var \ContextualCode\EzPlatformContentVariablesBundle\Entity\Variable[] $variables */
        $variables = $this->variableHandler->findAll();

        $replacementFrom = [];
        $replacementTo = [];
        foreach ($variables as $variable) {
            $placeholder = $variable->getPlaceholder();
            if ($placeholder === null || strpos($content, $placeholder) === false) {
                continue;
            }

            $value = $this->variableHandler->getVariableValue($variable);
            if ($value === null) {
                continue;
            }
            $replacementFrom[] = $placeholder;
            $replacementTo[] = $value;
        }
        if ($replacementFrom) {
            $content = str_replace($replacementFrom, $replacementTo, $content);
        }

        return $content;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => ['onKernelResponse', -5],
        ];
    }
}
